﻿using D247Microservice.SharedKernel.Common;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Net;
using System.Text;

namespace D247Microservice.Infrastructure.GlobalExceptionMiddleware
{
    public class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public GlobalExceptionMiddleware(RequestDelegate next, ILogger logger)
        {
            _logger = logger;
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.Error($"Something went wrong: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            HttpStatusCode httpStatus = HttpStatusCode.OK;
            if (Enum.IsDefined(typeof(HttpStatusCode), context.Response.StatusCode))
            {
                if (context.Response != null)
                    httpStatus = (HttpStatusCode)(Enum.ToObject(typeof(HttpStatusCode), context.Response.StatusCode));
            }

            string strResponse = new DefaultError()
            {
                ErrorCode = httpStatus,
                Message = exception.Message
            }.ToString();

            byte[] responseBytes = Encoding.ASCII.GetBytes(strResponse);

            await context.Response.Body.WriteAsync(responseBytes);
        }
    }
}
