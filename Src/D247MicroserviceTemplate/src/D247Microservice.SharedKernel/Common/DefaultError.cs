﻿using Newtonsoft.Json;
using System.Net;

namespace D247Microservice.SharedKernel.Common
{
    public class DefaultError
    {
        public HttpStatusCode ErrorCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
