using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Models;
using D247Microservice.Web.Mapper;
using D247Microservice.Infrastructure.GlobalExceptionMiddleware;

var configuration = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json")
               .Build();
Log.Logger = new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .CreateLogger();
var builder = WebApplication.CreateBuilder(args);
builder.Logging.ClearProviders();
builder.Host.UseSerilog();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddApiVersioning(options => {
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ReportApiVersions = true;
});

builder.Services.AddVersionedApiExplorer(options => {
    options.GroupNameFormat = "'v'VVV";
    options.SubstituteApiVersionInUrl = true;
});

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "D247 Microservice Template",
        Version = "v1",
        Description = "API for D247Microservice",
        TermsOfService = new Uri("https://www.google.com"),
        Contact = new OpenApiContact { Name = "Deluxe Corporation", Email = "abc@Deluxe.com", Url = new Uri("https://www.google.com") }
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
    options.EnableAnnotations();
});

builder.Services.AddMediatR(typeof(Program).GetTypeInfo().Assembly);
builder.Services.AddAutoMapper(typeof(MapperProfile).Assembly);
builder.Services.AddSingleton(Log.Logger);
builder.Services.ConfigureApplicationCookie(options =>
{
    options.Cookie.SameSite = SameSiteMode.Lax;
});
builder.Services.AddCors(c =>
{
    c.AddPolicy("AllowCORS", options =>
    {
        //options.WithOrigins("http://csrf.local.net");
        //options.AllowAnyHeader().WithExposedHeaders("X-XSRF-TOKEN", "Set-Cookie");
        //options.AllowCredentials();
        options.AllowAnyOrigin();
        options.AllowAnyHeader();
        options.AllowAnyMethod();
    });
});
var app = builder.Build();
app.UseSerilogRequestLogging();
//Register global exception handler at start to catch exception if occure in request processing.
app.UseMiddleware<GlobalExceptionMiddleware>();
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    var provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
    foreach (var description in provider.ApiVersionDescriptions)
    {
        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
    }
});
app.UseCors("AllowCORS");

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseSerilogRequestLogging();
app.Run();